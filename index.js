// 1. DOM - объектная модель документа.
// 2. innerText выводит только текст, игнорируя все элементы, а innerHTML покажет
// текстовую информацию по всему элементу.
// 3. getElementById, getElementsByTagName/ClassName/Name, querySelectorAll, querySelector.
// Лучше использовать способы querySelectorAll, querySelector.

// const getParagraphs = document.getElementsByTagName('p');

// const changeColor = function () {
//     [...getParagraphs].forEach(paragraph => {
//     paragraph.style.backgroundColor = "#ff0000";
//     });
// }

// changeColor();

const getParagraphs = document.querySelectorAll("p");

getParagraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = "#ff0000";
});

const findElem = document.getElementById("optionsList");
console.log(findElem);

const findElemParent = findElem.parentNode;
console.log(findElemParent);

const findElemChildren = findElem.childNodes;
console.log(findElemChildren);

const findElemChildrenNodeType = findElem.nodeType;
console.log(findElemChildrenNodeType);

const findElemChildrenNodeName = findElem.nodeName;
console.log(findElemChildrenNodeName);

const changeText = document.querySelector("#testParagraph");
changeText.textContent = "This is a paragraph";
console.log(changeText);

const getElem = document.querySelector(".main-header");
console.log(getElem);

const getChildren = getElem.children;
[...getElem.children].forEach(elem => {
    elem.classList.add("nav-item");
});
console.log(getChildren);

const findSectionTitle = document.querySelectorAll(".section-title");
console.log(findSectionTitle);

[...findSectionTitle].forEach(elem => {
    elem.classList.remove("section-title");
});
console.log(findSectionTitle);




